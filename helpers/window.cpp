#include "window.h"
#include "helper.h"
#include "maze.h"
#include <SFML/Graphics.hpp>
#include "creaturesClass.h"
#include <string.h>
#include <time.h>

#include <stdio.h>

GameWindow::GameWindow(const unsigned short& _screenWidth, const unsigned short& _screenHeight, const char* const _appName, Creatures& _creature)
	:	screenWidth(Helper::max(Helper::min(MAX_SCREEN_WIDTH, _screenWidth), MIN_SCREEN_WIDTH)),
		screenHeight(Helper::min(MAX_SCREEN_HEIGHT, _screenHeight)),
		creatures(_creature)
{
	_init(_appName);
}

void GameWindow::setText(sf::Text& text, const char* str) {
	text.setString(str);
	text.setCharacterSize(24);
	text.setColor(sf::Color::Yellow);
	text.setFont(robotoFont);
}

void GameWindow::_init(const char* const _appName) {
	#pragma region create render window
	unsigned int _appNameLen = Helper::strlen(_appName);
	if (5 < _appNameLen && _appNameLen < MAX_APP_NAME_LENGTH - 1) {
		memcpy(appNameArr, _appName, _appNameLen + 1);
	} else {
		memcpy(appNameArr, APP_HEADER, Helper::strlen(APP_HEADER) + 1);
	}
	gameRenderWindow = std::unique_ptr<sf::RenderWindow>(new sf::RenderWindow(sf::VideoMode(screenWidth, screenHeight), appName, sf::Style::Titlebar | sf::Style::Close));
	#pragma endregion

	#pragma region box init
	boxBorder.setFillColor(sf::Color::Blue);
	fence.setFillColor(sf::Color::White);
	boxEmpty.setFillColor(sf::Color::Black);


	food.setFillColor(sf::Color::Yellow);
	food.setOutlineThickness(CIRCLE_SIZE);
	food.setOutlineColor(sf::Color::Black);

	foodBig.setFillColor(sf::Color::Yellow);
	foodBig.setOutlineThickness(CIRCLE_SIZE);
	foodBig.setOutlineColor(sf::Color::Yellow);
	#pragma endregion

	#pragma region text init
	arialFont.loadFromFile("data/arial.ttf");
	robotoFont.loadFromFile("data/roboto.ttf");
	robotoBoldFont.loadFromFile("data/Roboto-Bold.ttf");

	setText(lifesPrefix, "Lifes: ");
	setText(pointsPrefix, "Points: ");
	setText(lifesNumber, "");
	setText(pointsNumber, "");

	loseMessage.setPosition(screenWidth / 2 - 71, screenHeight / 2 - 30);
	loseMessage.setCharacterSize(37);
	loseMessage.setColor(sf::Color::Red);
	loseMessage.setFont(robotoBoldFont);
	loseMessage.setStyle(sf::Text::Bold | sf::Text::Underlined);
	#pragma endregion
}

void GameWindow::drawBox(const char& cell, const int& posX, const int& posY) {
	sf::Shape* shape = &boxEmpty;
	float correctionX = 0;
	float correctionY = 0;

	if (cell == '#') { // border
		shape = &boxBorder;
	} else if (cell == '-') { // fence
		correctionY = (3*SQUARE_SIZE)/8;
		shape = &fence;
	}  else if (cell == '.') { // normal food
		correctionX = CIRCLE_SIZE;
		correctionY = CIRCLE_SIZE;
		shape = &food;
	} else if (cell == 'O') { // big food
		correctionX = CIRCLE_SIZE;
		correctionY = CIRCLE_SIZE;
		shape = &foodBig;
	} else if (creatures.isCreature(cell)) {
		shape = &(creatures.getCreature(cell).shape);
	}

	(*shape).setPosition(posX + correctionX, posY + correctionY);
	gameRenderWindow->draw(*shape);
}

void GameWindow::init () {}

void GameWindow::draw(const Maze& maze, const unsigned short& numberOfLives, const unsigned short& points) {
	gameRenderWindow->clear();

	#pragma region draw maze
	for (int p = 0; p < maze.height; p++) {
		for (int j = 0; j < maze.width; j++) {
			drawBox(maze.map[p][j], j*SQUARE_SIZE, p*SQUARE_SIZE);
		}
	}
	#pragma endregion

	#pragma region draw lifes and points
	if (!initialisedTextPositions) {
		initialisedTextPositions = true;
		const unsigned short offsetTop = (maze.height + 1)*SQUARE_SIZE;
		const unsigned short offsetLeft = (3)*SQUARE_SIZE;
		const unsigned short offsetRight = (maze.width)*SQUARE_SIZE;
		lifesPrefix.setPosition(offsetLeft, offsetTop);
		lifesNumber.setPosition(offsetLeft + 100, offsetTop);
		pointsPrefix.setPosition(offsetRight - 2*offsetLeft - 13, offsetTop);
		pointsNumber.setPosition(offsetRight - 4*13, offsetTop);	
	}
	lifesNumber.setString(std::to_string(numberOfLives));
	pointsNumber.setString(std::to_string(points));

	gameRenderWindow->draw(lifesPrefix);
	gameRenderWindow->draw(lifesNumber);
	gameRenderWindow->draw(pointsPrefix);
	gameRenderWindow->draw(pointsNumber);
	#pragma endregion

	gameRenderWindow->display();
}

bool GameWindow::hasEventToHandle(sf::Event& event) {
	return (*gameRenderWindow).pollEvent(event);
}

void GameWindow::printLoseMessage(const char messageType) {
	switch (messageType)
	{
		case 'l':
			loseMessage.setString("Game Over! :/");
			break;
		case 'w':
			loseMessage.setString("You Win!");
			break;
		default:
			loseMessage.setString("Oooops!");
			break;
	}

	gameRenderWindow->draw(loseMessage);
	gameRenderWindow->display();

	clock_t now = clock();
	clock_t timeToStop = now + 3*1000;
	while (now < timeToStop) {
		now = clock();
	}
}