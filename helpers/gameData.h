#ifndef GAMEDATA_H_INCLUDED
#define GAMEDATA_H_INCLUDED

#include "window.h"
#include "maze.h"
#include "creaturesClass.h"
#include "rapidjson/document.h"
#include <SFML/Graphics.hpp>
#include <map>

class GameData {
	private:
		bool play = true;

		maze_type map = { { 0 } };
		Maze maze;
		bool bfsArrayVisited[MAZE_MAX_HEIGTH][MAZE_MAX_WIDTH] = { { 0 } };
		Point bfsArrayPath[MAZE_MAX_HEIGTH][MAZE_MAX_WIDTH];

		Creatures& creatures;
		std::map<const char, Teleport> teleports;
		Point fences[10];
		unsigned short numberOfFences = 0;

		const unsigned short width;
		const unsigned short height;

		#pragma region is cell special
		bool isPacman(const unsigned short&, const unsigned short&);
		bool isGhost(const unsigned short&, const unsigned short&);
		bool isTeleport(const unsigned short&, const unsigned short&);
		bool isBigFood(const unsigned short&, const unsigned short&);
		#pragma endregion

		Pacman& getPacman(void);
		void getNewCoordinatees(unsigned short&, unsigned short&, const unsigned short&);
		bool checkMove(const unsigned short&, const unsigned short&);
		void move(Creature&, unsigned short&, unsigned short&, const bool);
		void findMoveBots(Ghost&, Point&);
		bool searchWithBFS(GhostFollow&);
		bool bfs(const unsigned short&, const unsigned short&, const unsigned short&, const unsigned short&, Point*, unsigned short&);
		void loseLife(void);
		//void (*loseLifeMessagePrinter)(bool);
		//void (GameWindow::*loseLifeMessagePrinter2)(char);
		bool justLostLife = false;
		GameWindow& gameWindow;

	public:
		GameData(const rapidjson::Value&, const unsigned short, const unsigned short, Creatures&, GameWindow&);
		//void (GameWindow::*)(char));
		void stopGame(void);
		bool isPlaying(void);
		const Maze& getMaze(void);
		void handleEvents(const sf::Event);
		void moveBots(void);
		const unsigned short& getPointsReference(void);
		const unsigned short& getLifesReference(void);
};

#endif // GAMEDATA_H_INCLUDED
