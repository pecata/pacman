#include "parseFile.h"
#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include <cstdio>

ParseFile::ParseFile(void) {}

rapidjson::Document ParseFile::readFile(const char* filename) {
	FILE* fp = fopen(filename, "rb"); // non-Windows use "r"
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
	rapidjson::Document d;
	d.ParseStream(is);
	fclose(fp);
	return d;
}
