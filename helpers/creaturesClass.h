#ifndef CREATURES_CLASS_H_INCLUDED
#define CREATURES_CLASS_H_INCLUDED

#include "creature.h"

#define CREATURES_LIST_LENGTH_DEFINED 5

class Creatures {
	private:
		Pacman pacman;
		GhostFollow ghostBlue; // b
		Ghost ghostRed; // r
		Ghost ghostGreen; // g
		Ghost ghostYellow; // y

	public:
		const unsigned short CreaturesListLength = CREATURES_LIST_LENGTH_DEFINED;
		const char CreaturesList[CREATURES_LIST_LENGTH_DEFINED + 1] = "Pbgry";
		Creatures(void);
		Creature& getCreature(const char cell);
		bool isCreature(const char&);
};

#endif // CREATURES_CLASS_H_INCLUDED
