#ifndef INIT_H_INCLUDED
#define INIT_H_INCLUDED

#include "rapidjson/document.h"
#include "config.h"
#include "window.h"
#include "parseFile.h"
#include "creaturesClass.h"
#include "gameData.h"

class Init {
	private:
		rapidjson::Document configFile;
		rapidjson::Document levelFile;

		void setConfigData(void);
		void setGameWindow(void);

	public:
		ParseFile parseFile;
		Config* config;
		GameWindow* gameWindow;
		GameData* gameData;
		Creatures creatures = Creatures();

		Init(void);
		void readConfig(void);
		void readConfig(const char*);
		void readLevel(void);
		void readLevel(const char*);
};


#endif // INIT_H_INCLUDED
