#include "helper.h"
#include <string.h>

unsigned short Helper::min(const unsigned short& a, const unsigned short& b) {
	return b ^ ((a ^ b) & -(a < b));
}

unsigned short Helper::max(const unsigned short& a, const unsigned short& b) {
	return a ^ ((a ^ b) & -(a < b));
}

unsigned int Helper::strlen(const char* a) {
	unsigned int len = -1;
	while (a[++len]);
	return len;
}

void Helper::concatenate(const char* from, char* to) {
	
	const unsigned int toLen = Helper::strlen(to);
	
	/*
	unsigned int current = 0;
	while (from[current]) {
		to[toLen + current] = from[current];
		current++;
	}
	to[toLen + current] = 0;
	*/

	const unsigned int fromLen = Helper::strlen(from);
	memcpy(to + toLen, from, fromLen);
	to[toLen + fromLen] = 0;
}
