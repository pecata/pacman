#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

class Config {
	private:
		static const short MAX_LEVEL_LENGTH = 101;

		const bool DEFAULT_START_LEVEL_SELECT = false;
		const char* const DEFAULT_LEVEL_FILENAME_PREFIX = "level-%30d.json";
		char levelPrefixArr[MAX_LEVEL_LENGTH] = { 0 };

		unsigned int defaultWaitForBotTime = 250 * 2; // in miliseconds
		
		void copyDefaultLevelPrefix(void);

	public:
		const bool startLevelSelect;
		const char* const levelPrefix = levelPrefixArr;
		const unsigned int& waitForBotMoveTime = defaultWaitForBotTime;

		Config();
		Config(const bool, const char* const);
};

#endif // CONFIG_H_INCLUDED
